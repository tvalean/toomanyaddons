require 'zip'

Zip::File.open("build/TooManyAddons.zip", create: true) {
  |zipfile|
  zipfile.mkdir("TooManyAddons")
  zipfile.add("TooManyAddons/TooManyAddons.lua", "TooManyAddons.lua")
  zipfile.add("TooManyAddons/TooManyAddonsInterface.lua", "TooManyAddonsInterface.lua")
  zipfile.add("TooManyAddons/TooManyAddons.toc", "TooManyAddons.toc")
  zipfile.add("TooManyAddons/Documentation.txt", "Documentation.txt")
  zipfile.close()
}