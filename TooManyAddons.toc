## Interface: 110000
## Title: TooManyAddons
## Notes: Addon Manager
## Author: Tvalean, Dax006 (Original Author)
## Version: 11.0.0.0
## Dependencies:
## SavedVariables: TMAsettings,TMAdebug
## SavedVariablesPerCharacter: TMAprofiles, TMAlastgloballoaded
## X-Category: Interface Enhancements
## X-Curse-Packaged-Version: release
## X-Curse-Project-Name: TooManyAddons
## X-Curse-Project-ID: too-many-addons
TooManyAddonsInterface.lua
TooManyAddons.lua